package com.example.UCHeritage;

import java.util.ArrayList;

import com.example.UCHeritage.model.PoI;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

public class About extends Activity{
	
	private String language;
	private ArrayList<PoI> list;
	private String name;
	public static String source="0";

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        buildActionMenu();

        TextView c = (TextView) findViewById(R.id.textView);
        TextView w = (TextView) findViewById(R.id.titlePoI);
        TextView x = (TextView) findViewById(R.id.textView3);
        if(language.equals("PT")){
        	c.setText("\tEsta aplicação foi desenvolvida no âmbito da cadeira Mobilidades em Redes de Comunicações.\n\n" + "\tO seu objectivo é providenciar, às pessoas que visitem Coimbra, uma ferramenta capaz de oferecer informação interressante e fidedigna referente à Universidade de Coimbra.\n" + "\n\tEsta aplicação foi desenvolvida por João Sá, Tiago Mateus e Adriana Ferrugento.");
            w.setText("\tEsta barra superior mantém-se sempre no ecrã. O botão na margem direita permite o envio de um email para os criadores da aplicação, de forma a responder a dúvidas ou aceitar sugestões.\n");
            x.setText("\n\tA barra inferior possui 4 botões. O primeiro permite que o utilizador regresse menu inicial. O segundo redirecciona o utilizador para o mapa onde se encontram assinaladas as localizações dos diversos pontos de interesse. O terceiro permite pesquisar por um ponto de interesse. Por fim, o quarto botão possibilita a alteração de algumas definições, designadamente o idioma, e o utilizador ativo.");
        }
        else{
        	c.setText("\tThis application was developed within the course of Mobility in Communication Networks.\n\n\tThe goal of this app is to guide the people who are visiting Coimbra, and whose intention is to gather some interesting information about its University.\n\n\tThis app was developed by João Sá, Tiago Mateus and Adriana Ferrugento.");
        	w.setText("\tThis top bar is always active on the screen.  By clicking the button on the right, the user can send an email to the developers of this application, in order to ask questions or provide suggestions.");
        	x.setText("\n\tThe bottom bar has 4 buttons. The first one allows to user to return to the main menu. The second one redirects the user to a map that includes the different points of interest. The third one allows the search for a specific point of interest. Finally, the fourth lets the user advance to the settings menu where he can sign in and change the language.");
        }
        
    }

    /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/
}
