package com.example.UCHeritage;

import java.util.ArrayList;

import com.example.UCHeritage.model.PoI;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.VideoView;

public class UCMoreInfo extends Activity {
   
	private String language;
	private ArrayList<PoI> list;
	private String name;
	public static String source="0";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc_more_info);
        
        buildActionMenu();
        
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
		videoView.setVideoPath("file:///sdcard/Movies/Coimbra_in_TimeLapse.mp4");
		videoView.setMediaController(new MediaController(this));
		videoView.start();

        TextView texto = (TextView) findViewById(R.id.textView1);
        if(language.equals("PT")){
            texto.setText("\tA Universidade de Coimbra possui aproximadamente 20 mil estudantes, mantendo uma das maiores comunidades de estudantes internacionais em Portugal.\n\n"
                    +"\tActualmente a Universidade divide-se por três grandes pólos:\n\n"
                    +"\tA alta universitária, Pólo I, é onde se situam a reitoria e os serviços administrativos, que partilham o edifício histórico da Universidade com a Faculdade de Direito. Ainda na Alta Universitária, situam-se as Faculdades de Letras, de Psicologia e de Ciências e Tecnologia. Podemos também visitar o edifício da Biblioteca Geral e o Arquivo da Universidade.\n\n"
                    +"\tO Pólo II, ou Pólo da Engenharia, é onde se situam os departamentos de engenharia da FCTUC. Aqui encontramos também o Edifício Central, onde estão presentes os serviços académicos e alguns anfiteatros, bem como, as cantinas e as residências.\n\n"
                    +"\tO Pólo III, ou Pólo das Ciências da Saúde, refere-se à zona onde estão sediadas a Faculdade de Medicina e Faculdade de Farmácia. Nesta área podemos também encontrar alguns laboratórios de investigação associados a estas faculdades, como o CNC (Centro de Neurociência e Biologia Celular), CEF (Centro de Estudos Farmacêuticos), AIBILI (Association for Innovation and Biomedical Research on Light and Image), entre outros.\n\n"
                    +"\tA Faculdade de Economia situa-se num palacete da Avenida Dias da Silva e foi fundada em 1972.");
        }
        else{
            texto.setText("\tThe University of Coimbra has, approximately 20 thousand students, housing/sheltering one of the largest communities of international students in Portugal.\n\n"
                    +"\tNowadays the University spreads itself across 3 large Pólos:\n\n"
                    +"\tThe Alta Universitária, Pólo 1, is where the rectory and the administrative services are located, sharing the historical building of the University  with the Faculty of Law. Still in the Alta, we can find the Faculties of Arts and Humanities, Psychology and Sciences/Technology. The general library and the archive complete the set of places to visit.\n\n"
                    +"\tThe Pólo II, or Engineering Pólo, is where the departments of engineering of the Faculty of Sciences and Technology are located. There we can also find the Edifício Central, where the academic services and a few amphitheaters are located, as well as, cafeterias and residencies.\n\n"
                    +"\tThe Pólo III, or Health Sciences Pólo, is where the Faculty of Medicine and Pharmacy are located. There we can also find some research laboratories associated with these Faculties, like CNC (Center of Neuroscience and Cell Biology), CEF (Center of pharmaceutical studies), AIBILI (Association for Innovation and Biomedical Research on Light and Image), amongst others.\n\n"
                    +"\tThe Faculty of Economy is located in a mansion of Avenida Dias da Silva and was created in 1972.");
        }
    }
    
    /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/
    
    
}

