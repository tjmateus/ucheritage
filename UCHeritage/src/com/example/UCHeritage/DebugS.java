package com.example.UCHeritage;

import android.util.Log;

public final class DebugS{
    private DebugS (){}

    public static void out (Object msg){
        Log.i ("info", msg.toString ());
    }
}