package com.example.UCHeritage;

import java.util.ArrayList;

import org.json.JSONException;

import com.example.UCHeritage.model.PoI;
import com.example.UCHeritage.model.Weather;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

public class Main extends Activity {
	
	private static final String TextView = null;
    private TextView condDescr;
    private ImageView imgView;
    private TextView temp;
	private static boolean flag=false;
	public static ArrayList<PoI> list = new ArrayList<PoI>();
    public static TCPClient mTcpClient;
    private Drawable dr;
    private Intent intent;
    public static String tempo;
    public static String temperature;
    public static String conditions;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        if(isNetworkAvailable()){
        	String city = "Coimbra,PT";

            condDescr = (TextView) findViewById(R.id.condDescr);
            temp = (TextView) findViewById(R.id.temp);
            imgView = (ImageView) findViewById(R.id.condIcon);
            
        	JSONWeatherTask task = new JSONWeatherTask();
        	task.execute(new String[]{city});
        }
        
        // connect to the server
        new connectTask().execute();

        while (!flag) {
            try { Thread.sleep(1000); }
            catch (InterruptedException e) { e.printStackTrace(); }
        }

        
        
    }
    
    public class connectTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... message) {
            //we create a TCPClient object and
            mTcpClient = new TCPClient(new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public Void messageReceived(ArrayList<PoI> mens) {
                	list = mens;
                	flag=true;
					//mTcpClient.stopClient();
                	return null;
                }
            });
            mTcpClient.run();
			return null;
        }
    }
    
    private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {
		public WeatherHttpClient n;
	    @Override
	    protected Weather doInBackground(String... params) {
	        Weather weather = new Weather();
	        n = new WeatherHttpClient();
	        String data = ( n.getWeatherData(params[0]));
	
	        try {
	            weather = JSONWeatherParser.getWeather(data);
	
	            // Let's retrieve the icon
	            weather.iconData = (n.getImage(weather.currentCondition.getIcon()));
	
	        } catch (JSONException e) {
	            e.printStackTrace();
	        }
	        return weather;
	
	    }
	
	    @Override
	    protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);
            
            if(weather.currentCondition.getWeatherId() >= 200 && weather.currentCondition.getWeatherId() <= 232){
                Main.tempo="thunderstorm";
                
            }
            else if(weather.currentCondition.getWeatherId() >= 300 && weather.currentCondition.getWeatherId() <= 321){
            	Main.tempo="drizzle";
            }
            else if(weather.currentCondition.getWeatherId() >= 500 && weather.currentCondition.getWeatherId() <= 522){
            	Main.tempo="rain";
            }
            else if(weather.currentCondition.getWeatherId() >= 600 && weather.currentCondition.getWeatherId() <= 621){
            	Main.tempo="snow";
            }
            else if(weather.currentCondition.getWeatherId() >= 701 && weather.currentCondition.getWeatherId() <= 741){
            	Main.tempo="fog";
            }
            else if(weather.currentCondition.getWeatherId() >= 801 && weather.currentCondition.getWeatherId() <= 804){
            	Main.tempo="clouds";
            }
            else if(weather.currentCondition.getWeatherId() == 800){
            	Main.tempo="clear";
            }
            Main.conditions=weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")";
            Main.temperature="" + Math.round((weather.temperature.getTemp() - 275.15)) + "ºC";
            
            n.close();
            
            Intent intent = new Intent(Main.this,Splash.class);
            intent.putExtra("language","PT");
            intent.putExtra("ListPoI",list);
            MainMenu.tempo=tempo;
            MainMenu.conditions=conditions;
            MainMenu.temperature=temperature;
            
            Main.this.startActivity(intent);
            Main.this.finish();
	    }
	}
    
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager 
              = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}




