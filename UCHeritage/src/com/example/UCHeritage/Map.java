package com.example.UCHeritage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.UCHeritage.model.PoI;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


@SuppressLint("DefaultLocale")
public class Map extends FragmentActivity {

	private GoogleMap map;
	private ArrayList<LatLng> markerPoints;
	private ArrayList <PoI> list;
	private String language; 
	private String source;
	private String name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		// Initializing 
		buildActionMenu();
		markerPoints = new ArrayList<LatLng>();
		this.source= MainMenu.source;
		
		// Getting reference to SupportMapFragment of the activity_main
		SupportMapFragment fm = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
		
		// Getting Map for the SupportMapFragment
		map = fm.getMap();	
		map.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(40.201428,-8.445446) , 12.0f) );
		
		addMarkers();
		
		if(map!=null){
			
			final Intent intent = new Intent(this, PoIAct.class);
			
			// Enable MyLocation Button in the Map
			map.setMyLocationEnabled(true);
			
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {          
	            public void onInfoWindowClick(Marker marker) 
	            {
	            	intent.putExtra("language",language);
	            	intent.putExtra("source",""+source);
	            	intent.putExtra("ListPoI",list);
	            	intent.putExtra("marker", marker.getTitle());
	            	intent.putExtra("map", "map");
	                startActivity(intent);
	            }
	        });
		}		
	}
	
	
	private void addMarkers(){
		
		int i;
		
		if(source.equals("0")){
			for(i=0; i<list.size(); i++){
				if(language.equals("PT"))
					map.addMarker(new MarkerOptions().position(new LatLng (list.get(i).getLatitude(),list.get(i).getLongitude())).title(list.get(i).getNome()).snippet(list.get(i).getDescricaoPT()).icon(BitmapDescriptorFactory.defaultMarker(getColor(list.get(i).getColor()))));
				else
					map.addMarker(new MarkerOptions().position(new LatLng (list.get(i).getLatitude(),list.get(i).getLongitude())).title(list.get(i).getNome()).snippet(list.get(i).getDescricaoEN()).icon(BitmapDescriptorFactory.defaultMarker(getColor(list.get(i).getColor()))));
			}
		}
		else{
			for(i=0; i<list.size(); i++){
				if(list.get(i).getCircuito()==Integer.parseInt(source)){
					map.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(list.get(i).getLatitude(),list.get(i).getLongitude()) , 16.0f) );
					if(language.equals("PT"))
						map.addMarker(new MarkerOptions().position(new LatLng (list.get(i).getLatitude(),list.get(i).getLongitude())).title(list.get(i).getNome()).snippet(list.get(i).getDescricaoPT()).icon(BitmapDescriptorFactory.defaultMarker(getColor(list.get(i).getColor()))));
					else
						map.addMarker(new MarkerOptions().position(new LatLng (list.get(i).getLatitude(),list.get(i).getLongitude())).title(list.get(i).getNome()).snippet(list.get(i).getDescricaoEN()).icon(BitmapDescriptorFactory.defaultMarker(getColor(list.get(i).getColor()))));
			
				}
			}
		}
		
		
	}
	
	private float getColor(String color){
		
		float c;
		
		if(color.toUpperCase().equals("AZURE"))
			c=BitmapDescriptorFactory.HUE_AZURE;
		else if(color.toUpperCase().equals("BLUE"))
			c=BitmapDescriptorFactory.HUE_BLUE;
		else if(color.toUpperCase().equals("CYAN"))
			c=BitmapDescriptorFactory.HUE_CYAN;
		else if(color.toUpperCase().equals("GREEN"))
			c=BitmapDescriptorFactory.HUE_GREEN;
		else if(color.toUpperCase().equals("MAGENTA"))
			c=BitmapDescriptorFactory.HUE_MAGENTA;
		else if(color.toUpperCase().equals("ORANGE"))
			c=BitmapDescriptorFactory.HUE_ORANGE;
		else if(color.toUpperCase().equals("RED"))
			c=BitmapDescriptorFactory.HUE_RED;
		else if(color.toUpperCase().equals("ROSE"))
			c=BitmapDescriptorFactory.HUE_ROSE;
		else if(color.toUpperCase().equals("VIOLET"))
			c=BitmapDescriptorFactory.HUE_VIOLET;
		else 
			c=BitmapDescriptorFactory.HUE_YELLOW;
		
		return c;
							
	}
	
	private String getDirectionsUrl(LatLng origin,LatLng dest){
					
		// Origin of route
		String str_origin = "origin="+origin.latitude+","+origin.longitude;
		
		// Destination of route
		String str_dest = "destination="+dest.latitude+","+dest.longitude;		
		
					
		// Sensor enabled
		String sensor = "sensor=false";			
					
		// Building the parameters to the web service
		String parameters = str_origin+"&"+str_dest+"&"+sensor;
					
		// Output format
		String output = "json";
		
		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
		
		
		return url;
	}
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url 
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url 
                urlConnection.connect();

                // Reading data from url 
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb  = new StringBuffer();

                String line = "";
                while( ( line = br.readLine())  != null){
                        sb.append(line);
                }
                
                data = sb.toString();

                br.close();

        }catch(Exception e){
                Log.d("Exception while downloading url", e.toString());
        }finally{
                iStream.close();
                urlConnection.disconnect();
        }
        return data;
     }

	
	
	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String>{			
				
		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {
				
			// For storing data from web service
			String data = "";
					
			try{
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			}catch(Exception e){
				Log.d("Background Task",e.toString());
			}
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);			
			
			ParserTask parserTask = new ParserTask();
			
			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);
				
		}		
	}
	
	/** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
    	
    	// Parsing the data in non-ui thread    	
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
			
			JSONObject jObject;	
			List<List<HashMap<String, String>>> routes = null;			           
            
            try{
            	jObject = new JSONObject(jsonData[0]);
            	DirectionsJSONParser parser = new DirectionsJSONParser();
            	
            	// Starts parsing data
            	routes = parser.parse(jObject);    
            }catch(Exception e){
            	e.printStackTrace();
            }
            return routes;
		}
		
		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			
			// Traversing through all the routes
			for(int i=0;i<result.size();i++){
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();
				
				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);
				
				// Fetching all the points in i-th route
				for(int j=0;j<path.size();j++){
					HashMap<String,String> point = path.get(j);					
					
					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);	
					
					points.add(position);						
				}
				
				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(2);
				lineOptions.color(Color.RED);	
				
			}
			
			// Drawing polyline in the Google Map for the i-th route
			map.addPolyline(lineOptions);							
		}			
    }   
    
    /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/
}
