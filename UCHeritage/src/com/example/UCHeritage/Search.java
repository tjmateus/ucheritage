package com.example.UCHeritage;

import java.util.ArrayList;

import com.example.UCHeritage.model.PoI;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

public class Search extends Activity{
	
	private String language;
	private ArrayList<PoI> list;
	private String query;
	private String source;
	private String name;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);
	     super.onCreate(savedInstanceState);
	     setContentView(R.layout.search);
	    
	     buildActionMenu();
	     updateLabels();
	     
	     Intent intent = getIntent();
	     TextView buttonSend = (TextView) findViewById(R.id.sear);
	     this.query = intent.getStringExtra("query");
	     buttonSend.setText(query);
	     
	     ListView lv = (ListView)findViewById(R.id.listPoI);
	     
	     final ArrayList<String> listPoIName = new ArrayList<String>();
	     for (int i = 0; i < list.size(); ++i) {
	    	 if(list.get(i).getNome().toLowerCase().contains(query.toLowerCase()))
	    		 listPoIName.add(list.get(i).getNome());
	     }
	     ArrayAdapter<String> myarrayAdapter = new ArrayAdapter<String>(this,R.layout.list_view_custom,R.id.list_content,listPoIName);
	     lv.setAdapter(myarrayAdapter);
	     lv.setTextFilterEnabled(true); 
	     
	     final Intent intentAct = new Intent(this,PoIAct.class);
	     intentAct.putExtra("language",language);
	     intentAct.putExtra("source",""+source);
	     intentAct.putExtra("name", name);
	     intentAct.putExtra("ListPoI",list);
	     
	     lv.setOnItemClickListener(new OnItemClickListener() {
	    	  @Override
	    	  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	    		  String selected = ((TextView) view.findViewById(R.id.list_content)).getText().toString();
	    		  intentAct.putExtra("marker",selected);
	    		  intentAct.putExtra("search", "search");
	    	      startActivity(intentAct);
	    	      finish();
	    	  }
	    }); 
	}
	
	private void updateLabels(){
		TextView search = (TextView) findViewById(R.id.textSearch);
		TextView list = (TextView) findViewById(R.id.listTextPoI);
		
		
		if(language.equals("PT")){
			search.setText("PROCURA:");
			list.setText("Pontos de Interesse:");
		}
		else{
			search.setText("SEARCH:");
			list.setText("Points of Interest:");
		}
	}
	
	/*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/
}
