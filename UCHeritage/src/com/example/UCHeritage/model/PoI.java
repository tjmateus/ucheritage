package com.example.UCHeritage.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PoI implements Serializable {
	
	private String nome;
	private String id;
	private int circuito;
	private int ordem;
	private boolean isLast;
	private String color;
	private double latitude;
	private double longitude;
	private ArrayList<Comentario> comentarios;
	private String descricaoPT;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	private String descricaoEN;
	private String textoPT;
	private String textoEN;

	public PoI(String nomeAux, String idAux, int circuitoAux, int ordemAux, boolean isLastAux, String colorAux, double latitudeAux, double longitudeAux, String descricaoPTAux, String descricaoENAux, String textoPTAux, String textoENAux){
		this.nome = nomeAux;
		this.id=idAux;
		this.circuito = circuitoAux;
		
		this.ordem=ordemAux;
		this.isLast=isLastAux;
		this.color=colorAux;
		
		this.latitude = latitudeAux;
		this.longitude = longitudeAux;
		
		this.comentarios = new ArrayList <Comentario> ();
		
		this.descricaoPT = descricaoPTAux;
		this.descricaoEN = descricaoENAux;
		
		this.textoPT = textoPTAux;
		this.textoEN = textoENAux;
		
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public boolean isLast() {
		return isLast;
	}

	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCircuito() {
		return circuito;
	}
	public void setCircuito(int circuito) {
		this.circuito = circuito;
	}

	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}
	public void setComentarios(ArrayList<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public String getDescricaoPT() {
		return descricaoPT;
	}
	public void setDescricaoPT(String descricaoPT) {
		this.descricaoPT = descricaoPT;
	}

	public String getDescricaoEN() {
		return descricaoEN;
	}
	public void setDescricaoEN(String descricaoEN) {
		this.descricaoEN = descricaoEN;
	}

	public String getTextoPT() {
		return textoPT;
	}
	public void setTextoPT(String textoPT) {
		this.textoPT = textoPT;
	}

	public String getTextoEN() {
		return textoEN;
	}
	public void setTextoEN(String textoEN) {
		this.textoEN = textoEN;
	}
}
