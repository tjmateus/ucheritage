package com.example.UCHeritage.model;

import java.io.Serializable;

public class Comentario implements Serializable {
	private String nome; 
	private String text;
	
	public Comentario(String nomeAux, String textAux){
		this.nome = nomeAux;
		this.text = textAux;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
