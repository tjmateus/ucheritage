package com.example.UCHeritage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.example.UCHeritage.model.PoI;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.plus.PlusClient;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;

public class SignInActivity extends Activity implements OnClickListener,
        PlusClient.ConnectionCallbacks, PlusClient.OnConnectionFailedListener,
        PlusClient.OnAccessRevokedListener {

    private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;

    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;
    
    private static final int IO_BUFFER_SIZE = 4 * 1024;

    private TextView mSignInStatus;
    private PlusClient mPlusClient;
    private SignInButton mSignInButton;
    private View mSignOutButton;
    private View mRevokeAccessButton;
    private ConnectionResult mConnectionResult;
    private String language;
    private ArrayList<PoI> list;
    private String name;
    public static String source="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);

        mPlusClient = new PlusClient.Builder(this, this, this)
                .setActions(MomentUtil.ACTIONS)
                .build();
        
        buildActionMenu();
        updateLabels();
        setListeners();

        mSignInStatus = (TextView) findViewById(R.id.sign_in_status);
        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(this);
        mSignOutButton = findViewById(R.id.sign_out_button);
        mSignOutButton.setOnClickListener(this);
        mRevokeAccessButton = findViewById(R.id.revoke_access_button);
        mRevokeAccessButton.setOnClickListener(this);
    }
    
 /*-----CRIADO MANUALMENTE-------*/
    
    private void setListeners(){
    	
    	Button button= (Button) findViewById(R.id.confirm);
    	button.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	Intent intent = new Intent(getBaseContext(), MainMenu.class);
    	    	
    	    	intent.putExtra("name", name);
    	    	
    	    	RadioButton radioPT = (RadioButton) findViewById(R.id.radio0);
            	RadioButton radioEN = (RadioButton) findViewById(R.id.radio1);
            	
    	    	if(radioPT.isChecked()){
    	    		intent.putExtra("language","PT");
    	    		intent.putExtra("ListPoI",list);
    	    	}
    	    	else{
    	    		intent.putExtra("language","EN");
    	    		intent.putExtra("ListPoI",list);
    	    	}
    	        startActivity(intent);
    	        finish();
    	    }
    	});
    }
    
    private void updateLabels(){
    	if(language.equals("PT")){
    		TextView label = (TextView) findViewById(R.id.language);
        	label.setText("Linguagem:");
        	
        	RadioButton radioPT = (RadioButton) findViewById(R.id.radio0);
        	RadioButton radioEN = (RadioButton) findViewById(R.id.radio1);
        	
        	radioPT.setChecked(true);
        	
        	radioPT.setText("Português");
        	radioEN.setText("Inglês");
        	
        	Button bt = (Button) findViewById(R.id.confirm);
        	bt.setText("Confirmar");
    	}
    	else{
    		TextView label = (TextView) findViewById(R.id.language);
        	label.setText("Language:");
        	
        	RadioButton radioPT = (RadioButton) findViewById(R.id.radio0);
        	RadioButton radioEN = (RadioButton) findViewById(R.id.radio1);
        	
        	radioEN.setChecked(true);
        	
        	radioPT.setText("Portuguese");
        	radioEN.setText("English");
        	
        	Button bt = (Button) findViewById(R.id.confirm);
        	bt.setText("Confirm");
    	}
    	new Intent(this, MainMenu.class).putExtra("name", name);
    }
    
    /*-------------------*/
    
    /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/

    @Override
    public void onStart() {
        super.onStart();
        mPlusClient.connect();
    }

    @Override
    public void onStop() {
        mPlusClient.disconnect();
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.sign_in_button:
                int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
                if (available != ConnectionResult.SUCCESS) {
                    showDialog(DIALOG_GET_GOOGLE_PLAY_SERVICES);
                    return;
                }
                
                PoIAct.isSignedIn=true;

                try {
                    mSignInStatus.setText(getString(R.string.signing_in_status));
                    if(mConnectionResult!=null)
                    	mConnectionResult.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    // Fetch a new result to start.
                    mPlusClient.connect();
                }
                break;
            case R.id.sign_out_button:
                if (mPlusClient.isConnected()) {
                    mPlusClient.clearDefaultAccount();
                    mPlusClient.disconnect();
                    mPlusClient.connect();
                    PoIAct.isSignedIn=false;
                }
                break;
            case R.id.revoke_access_button:
                if (mPlusClient.isConnected()) {
                    mPlusClient.revokeAccessAndDisconnect(this);
                    updateButtons(false /* isSignedIn */);
                }
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id != DIALOG_GET_GOOGLE_PLAY_SERVICES) {
            return super.onCreateDialog(id);
        }

        int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (available == ConnectionResult.SUCCESS) {
            return null;
        }
        if (GooglePlayServicesUtil.isUserRecoverableError(available)) {
            return GooglePlayServicesUtil.getErrorDialog(
                    available, this, REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES);
        }
        return new AlertDialog.Builder(this)
                .setMessage(R.string.plus_generic_error)
                .setCancelable(true)
                .create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SIGN_IN
                || requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
            if (resultCode == RESULT_OK && !mPlusClient.isConnected()
                    && !mPlusClient.isConnecting()) {
                mPlusClient.connect();
            }
        }
    }

    @Override
    public void onAccessRevoked(ConnectionResult status) {
        if (status.isSuccess()) {
            mSignInStatus.setText(R.string.revoke_access_status);
        } else {
            mSignInStatus.setText(R.string.revoke_access_error_status);
            mPlusClient.disconnect();
        }
        mPlusClient.connect();
    }

    @SuppressLint("NewApi")
	@Override
    public void onConnected(Bundle connectionHint) {
        String currentPersonName = mPlusClient.getCurrentPerson() != null
                ? mPlusClient.getCurrentPerson().getDisplayName()
                : getString(R.string.unknown_person);  
        mSignInStatus.setText(getString(R.string.signed_in_status, currentPersonName));
        if(mPlusClient.getCurrentPerson()!=null){
        	name=currentPersonName;
        	PoIAct.isSignedIn=true;
        }
        updateButtons(true /* isSignedIn */);
    }

    @Override
    public void onDisconnected() {
        mSignInStatus.setText(R.string.loading_status);
        mPlusClient.connect();
        updateButtons(false /* isSignedIn */);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mConnectionResult = result;
        updateButtons(false /* isSignedIn */);
    }

    private void updateButtons(boolean isSignedIn) {
        if (isSignedIn) {
        	mSignInButton.setEnabled(false);
            mSignOutButton.setEnabled(true);
            mRevokeAccessButton.setEnabled(true);
        } else {
            if (mConnectionResult == null) {
                // Disable the sign-in button until onConnectionFailed is called with result.
                mSignInButton.setVisibility(View.INVISIBLE);
                mSignInStatus.setText(getString(R.string.loading_status));
            } else {
                // Enable the sign-in button since a connection result is available.
                mSignInButton.setVisibility(View.VISIBLE);
                mSignInButton.setEnabled(true);
                mSignInStatus.setText(getString(R.string.signed_out_status));
            }
            mSignOutButton.setEnabled(false);
            mRevokeAccessButton.setEnabled(false);
        }
    }
}
