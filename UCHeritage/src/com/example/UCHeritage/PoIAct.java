package com.example.UCHeritage;

import java.util.ArrayList;

import com.example.UCHeritage.SimpleGestureFilter.SimpleGestureListener;
import com.example.UCHeritage.model.Comentario;
import com.example.UCHeritage.model.PoI;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class PoIAct extends Activity implements SimpleGestureListener{
	
	protected LocationManager locationManager;
	private String language;
	private ArrayList<PoI> list;
	private int source;
	private int position;
	private ImageButton next;
	private ImageButton previous;
	private ImageButton guide;
	private Button comment;
	private String name;
	private TextView textComment;
	private String title;
    private ListView lv;
    public static boolean isSignedIn=false;
    private SimpleGestureFilter detector;
	
	@SuppressLint("ShowToast")
	public void onCreate(Bundle savedInstanceState) {
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poi);
        
        next = (ImageButton) findViewById(R.id.next);
		previous = (ImageButton) findViewById(R.id.previous);
		guide = (ImageButton) findViewById(R.id.imageView1);
		comment = (Button) findViewById(R.id.comment_button);
		textComment = (TextView) findViewById(R.id.editText);
		lv = (ListView)findViewById(R.id.listComments);
		
		detector = new SimpleGestureFilter(this,this);
		
        buildActionMenu();
        getVariables();
        
        onClickGuide();
        
        if(getIntent().getStringExtra("map")!=null || getIntent().getStringExtra("search")!=null){
        	if(this.title!=null){
        		search();
        		updateLabels();
        		next.setVisibility(View.GONE);
        		previous.setVisibility(View.GONE);}
        }else{
        	buildCircuit();}
        
	     
	     
        
	}
	
	 @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        // Call onTouchEvent of SimpleGestureFilter class
         this.detector.onTouchEvent(me);
       return super.dispatchTouchEvent(me);
    }

	public void search(){
		for(int i=0; i<list.size(); i++){
			if(list.get(i).getNome().equals(this.title)){
				position=i;
				source=list.get(i).getCircuito();
			}
		}
		DebugS.out("Search "+position+" "+source);
	}
	
	public void onClickGuide(){
		
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                position++;
                lv.setAdapter(null);
                updateLabels();
            }
        });
        
        previous.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                position--;
                lv.setAdapter(null);
                updateLabels();
            }
        });
        
        guide.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(android.content.Intent.ACTION_VIEW, 
					    Uri.parse("http://maps.google.com/maps?daddr=" +
					    		list.get(position).getLatitude()+","+list.get(position).getLongitude()));
					startActivity(intent);
            }
        });
	}
	
	@SuppressWarnings("unchecked")
	private void getVariables(){
		 this.source= Integer.parseInt(MainMenu.source);
		 this.language=getIntent().getStringExtra("language");
		 this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
	     this.language = getIntent().getStringExtra("language");
	     this.name=getIntent().getStringExtra("name");
	     this.title=getIntent().getStringExtra("marker");
		
	}
	
	private void buildCircuit(){
		int i;
		DebugS.out("SOURCE "+source);
		for(i=0; i<list.size(); i++){
			if(list.get(i).getCircuito()==source){
				position=i;
				updateLabels();
				break;
			}
		}
	}
		
	private void updateLabels(){
		Button comment = (Button) findViewById(R.id.comment_button);
		TextView title = (TextView) findViewById(R.id.titlePoI);
		TextView text = (TextView) findViewById(R.id.textView1); 
		TextView textV = (TextView) findViewById(R.id.textV);
		ImageButton picture = (ImageButton) findViewById(R.id.imageView1);
		TextView clicar = (TextView) findViewById(R.id.textView2);
	
		textComment.clearFocus();
		
		if(language.equals("PT"))
			clicar.setText("(Clique na imagem para obter direções)");
		else
			clicar.setText("(Click on image to obtain directions)");
		
		final ArrayList<String> listPoIName = new ArrayList<String>();
	    for (int i = 0; i < list.get(position).getComentarios().size(); ++i) {
	    	listPoIName.add(list.get(position).getComentarios().get(i).getNome()+": "+ list.get(position).getComentarios().get(i).getText());
	    }
	    final ArrayAdapter<String> myarrayAdapter = new ArrayAdapter<String>(this,R.layout.list_view_custom,R.id.list_content,listPoIName);
	    lv.setAdapter(myarrayAdapter);
	    lv.setTextFilterEnabled(true); 
	     
	    lv.setOnTouchListener(new OnTouchListener() {
	    	    // Setting on Touch Listener for handling the touch inside ScrollView
	    	    @Override
	    	    public boolean onTouch(View v, MotionEvent event) {
	    	    // Disallow the touch request for parent scroll on touch of child view
	    	    v.getParent().requestDisallowInterceptTouchEvent(true);
	    	    return false;
	    	    }

			
	    });
	    
	    comment.setOnClickListener(new View.OnClickListener() {
         @SuppressLint("ShowToast")
        public void onClick(View v) {
          	String text = textComment.getText().toString();
          	
          	if(!isSignedIn || name==null){
          		if(language.equals("PT")){
          			Toast.makeText(getApplicationContext(), "Efectue o login primeiro.", 5).show();
          		}else{
          			Toast.makeText(getApplicationContext(), "Sign in first.", 5).show();
          		}
          		textComment.setText("");
          	}
          	else{
          		lv.setAdapter(null);
          		Comentario novo = new Comentario(name, text);
          		textComment.setText("");
              	list.get(position).getComentarios().add(novo);
              	Main.mTcpClient.sendMessage(list);
              	listPoIName.add(novo.getNome()+": "+ novo.getText());
              	lv.setAdapter(myarrayAdapter);
           }
          	
          }
      });
		
		if(list.get(position).getOrdem()==1)
			previous.setVisibility(View.GONE);
		else if(list.get(position).isLast()==true)
			next.setVisibility(View.GONE);
		else{
			previous.setVisibility(1);
			next.setVisibility(1);
		}

		Drawable image = getResources().getDrawable(getResources().getIdentifier(list.get(position).getId(),"drawable", getPackageName()));
		picture.setBackgroundDrawable(image);
		
		title.setText(list.get(position).getNome()); 
		if(this.language.equals("PT")){
			 comment.setText("Comentar");
			 textV.setText("Comentários:");
			 text.setText(list.get(position).getTextoPT());
		}
		else{
			comment.setText("Comment");
			textV.setText("Comments:");
			text.setText(list.get(position).getTextoEN());
		}
	}
	
	 /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }
    /*------------------*/

	@Override
	public void onSwipe(int direction) {
		switch (direction) {
	      
	      case SimpleGestureFilter.SWIPE_RIGHT :
	    	  if(list.get(position).getOrdem()!=1){
		    	  position--;
	              lv.setAdapter(null);
	              updateLabels();}
	          break;
	      case SimpleGestureFilter.SWIPE_LEFT :  
	    	  if(!list.get(position).isLast()){
		    	  position++;
	              lv.setAdapter(null);
	              updateLabels();}
	          break;
	      
	      }
		
	}

	@Override
	public void onDoubleTap() {
		// TODO Auto-generated method stub
		
	}
    
    

}
