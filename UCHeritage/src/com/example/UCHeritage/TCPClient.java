package com.example.UCHeritage;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

import android.widget.TextView;

import com.example.UCHeritage.model.PoI;


public class TCPClient {

    public static final String SERVERIP = "10.16.1.130";//"192.168.1.70"; //your computer IP address
    public static final int SERVERPORT = 4005;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;
    private ObjectInputStream console = null;
    private ObjectOutputStream streamOut = null;
    PrintWriter out;
    BufferedReader in;

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     * //@param message text entered by client
     */
    public Object receiveMessage(){
    	try {
            return console.readUTF();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
    
    public void sendMessage(ArrayList<PoI> message){
        if (streamOut != null) {
            try {
                streamOut.writeObject(message);
                
            } catch (IOException e) {
            	e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }

    public void stopClient(){
        mRun = false;
    }

    public void run() {
        mRun = true;
        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVERIP);
            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVERPORT);

            try {
                streamOut = new ObjectOutputStream(new DataOutputStream(socket.getOutputStream()));
                streamOut.flush();
                
                console = new ObjectInputStream(new DataInputStream(socket.getInputStream()));

                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    ArrayList<PoI> n = (ArrayList<PoI>)console.readObject();
                    mMessageListener.messageReceived(n);
                }
      
            } catch (Exception e) {
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
		Void messageReceived(ArrayList<PoI> mens);
    }
}
