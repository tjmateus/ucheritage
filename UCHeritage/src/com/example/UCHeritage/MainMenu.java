package com.example.UCHeritage;

//APIKEY AIzaSyA8dvsJWoM7uuAN1gw10K8OiEeYnaPtAOQ

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ClipData.Item;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;

import org.json.JSONException;

import com.example.UCHeritage.model.PoI;
import com.example.UCHeritage.model.Weather;

public class MainMenu extends Activity{
	
    private TextView labelText;
    private TextView infoGuide;
    private ImageButton button_play_info;
    private String language;
    private ArrayList<PoI> list;
    private String name = null;
    public static String source="0";
    private TextView condDescr;
    private ImageView imgView;
    private TextView temp;
    private Drawable dr;
    public static String tempo;
    public static String conditions;
    public static String temperature;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        
        condDescr = (TextView) findViewById(R.id.condDescr);
        temp = (TextView) findViewById(R.id.temp);
        imgView = (ImageView) findViewById(R.id.condIcon);
        
        weather();
        
        DebugS.out("tempo "+tempo);
        DebugS.out("dr: "+dr);
       
	    imgView.setImageDrawable(dr);
	    condDescr.setText(conditions);
	    temp.setText(temperature);
	    
    	buildActionMenu();
    	updateLabels();
        onClickGuide();

    }
    
    private void weather(){
    	if(tempo!=null && tempo.equals("thunderstorm")){
    		dr = getResources().getDrawable(R.drawable.thunderstorm);
        }
        else if(tempo!=null && tempo.equals("drizzle")){
        	dr = getResources().getDrawable(R.drawable.drizzle);
        }
        else if(tempo!=null && tempo.equals("rain")){
            dr = getResources().getDrawable(R.drawable.rain);
        }
        else if(tempo!=null && tempo.equals("snow")){
            dr = getResources().getDrawable(R.drawable.snow);
        }
        else if(tempo!=null && tempo.equals("fog")){
            dr = getResources().getDrawable(R.drawable.fog);
        }
        else if(tempo!=null && tempo.equals("clouds")){
            dr = getResources().getDrawable(R.drawable.clouds);
        }
        else if(tempo!=null && tempo.equals("clear")){
            dr = getResources().getDrawable(R.drawable.clear);
        }
    }
    
    private void updateLabels(){
    	labelText = (TextView) findViewById(R.id.label_play_info);
    	infoGuide = (TextView) findViewById(R.id.info);
    	if(language.equals("PT")){
        	labelText.setText("MAIS INFORMAÇÃO");
        	infoGuide.setText("\tA UNIVERSIDADE DE COIMBRA é das universidades mais antigas ainda em operaçãoo e a mais antiga em Portugal." +
        			"\n\n\tEstá organizada em 8 faculdades diferentes de acordo com uma variedade de campos de conhecimento." +
        			"\n\n\tA universidade oferece todos os graus académicos em arquitetura, educação, engenharia, humanidades, direito, matemática, medicina, ciências naturais, psicologia, ciências sociais e desporto.");
    	}
    	else{
    		labelText.setText("MORE INFO");
    		infoGuide.setText("\tThe UNIVERSITY OF COIMBRA is one of oldest universities in continuous operation in the world, and the oldest university of Portugal." +
    				"\n\n\tIt is organized in 8 different faculties according to a variety of fields of knowledge." +
    				"\n\n\tThe university offers academic degrees in architecture, education, engineering, humanity, law, mathematics, medicine, natural sciences, psychology, social sciences and sports.");
    	}
    	
    }

    private void onClickGuide(){
    	final Intent intentUCMoreInfo = new Intent(this,UCMoreInfo.class);
    	intentUCMoreInfo.putExtra("language",language);
    	intentUCMoreInfo.putExtra("ListPoI",list);
    	
    	final Intent intentGuide = new Intent(this,PoIAct.class);
    	
    	
        button_play_info  = (ImageButton) findViewById(R.id.play_info);
        button_play_info.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	DebugS.out(labelText.getText());
                if(labelText.getText().equals("MAIS INFORMAÇÃO") || labelText.getText().equals("MORE INFO")){
                	intentGuide.putExtra("language",language);
                	intentGuide.putExtra("ListPoI",list);
                	intentGuide.putExtra("name", name);
                	intentGuide.putExtra("source",""+source);
                	startActivity(intentUCMoreInfo);
                }
                else{
                	intentGuide.putExtra("language",language);
                	intentGuide.putExtra("ListPoI",list);
                	intentGuide.putExtra("name", name);
                	intentGuide.putExtra("source",""+source);
                	startActivity(intentGuide);
                }
            }
        });

        ImageButton buttonUC = (ImageButton) findViewById(R.id.imageView);
        buttonUC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                maisInformacao();
                infoGuide = (TextView) findViewById(R.id.info);
                if(language.equals("PT")){
                	infoGuide.setText("\tA UNIVERSIDADE DE COIMBRA é das universidades mais antigas ainda em operação e a mais antiga em Portugal." +
                			"\n\n\tEstá organizada em 8 faculdades diferentes de acordo com uma variedade de campos de conhecimento." +
                			"\n\n\tA universidade oferece todos os graus académicos em arquitetura, educação, engenharia, humanidades, direito, matemática, medicina, ciências naturais, psicologia, ciências sociais e desporto.");
            	}
            	else{
            		infoGuide.setText("\tThe UNIVERSITY OF COIMBRA is one of the oldest still running, and the oldest in Portugal." +
            				"\n\n\tIt's organized in 8 different faculties according to a variety of fields of knowledge." +
            				"\n\n\tThe university offers every academic degrees in architecture, education, engineering, humanity, law, mathematics, medicine, natural sciences, psychology, social sciences and sport.");
            	}
            }
        });

        ImageButton buttonPaco = (ImageButton) findViewById(R.id.paco);
        buttonPaco.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	source="1";
                iniciarPercurso();
                infoGuide = (TextView) findViewById(R.id.info);
                if(language.equals("PT")){
                	infoGuide.setText("\tNeste circuito iremos percorrer uma zona denominada de PAÇO DAS ESCOLAS. " +
                            "\n\n\tOs pontos de maior interesse a visitar são a Sala dos Capelos, a Sala dos Archeiros, a Via Latina, a Porta Férrea, a Capela de São Miguel, a Biblioteca Joanina, a Prisão Académica e o Pátio e o Paço das Escolas.");
                }
                else{
                	infoGuide.setText("\tIn this route we are going to visit a region called PAÇO DAS ESCOLAS." +
                			"\n\n\tThe major points of interest are the Sala dos Capelos, the Salas dos Archeiros, the Via Latina, the Porta Férrea, the Capela de São Miguel, the Biblioteca Joanina, the Prisão Académica and the Pátio e Paço das Escolas.");
                }
                
            }
        });

        ImageButton buttonAlta = (ImageButton) findViewById(R.id.alta);
        buttonAlta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	source="2";
                iniciarPercurso();
                infoGuide = (TextView) findViewById(R.id.info);
                if(language.equals("PT")){
                	infoGuide.setText("\tNeste percurso vamos visitar a zona da Universidade de Coimbra a que se dá o nome de ALTA. " +
                            "\n\n\tOs pontos de maior interesse são a Igreja Sé Nova, a Igreja Sé Velha, o Museu Machado Castro, o Quebra Costas e, por fim, o Arco de Almedina.");
                }
                else{
                	infoGuide.setText("\tIn this route we are going to visit the region of the University of Coimbra called ALTA." +
                			"\n\n\tThe major points of interest are the Sé Nova church, the Sé Velha church, the Machado Castro Museum, the Quebra Costas and, finally, the Arco de Almedina.");
                }
                
            }
        });

        ImageButton buttonPolo1 = (ImageButton) findViewById(R.id.polo1);
        buttonPolo1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	source="3";
                iniciarPercurso();
                infoGuide = (TextView) findViewById(R.id.info);
                if(language.equals("PT")){
                	infoGuide.setText("\tNeste percurso iremos visitar o famoso PÓLO 1 onde se encontram a maior parte das Faculdades da Universidade de Coimbra. " +
                            "\n\n\tAqui se encontram a Faculdade de Direito, de Letras, de Psicologia, bem como, pertencentes à Faculdade de Ciências e Tecnologias, os Departamentos de Antropologia, de Biologia, de Física, de Química, de Matemática e de Arquitectura." +
                            "\n\n\tO Largo de D.Dinis mantém-se, ainda assim, como principal ponto turístico desta zona da Universidade.");
                }
                else{
                	infoGuide.setText("\tIn this route we are going to visit the famous PÓLO 1 where we can find most of the faculties of the University of Coimbra." +
                			"\n\n\tHere we can find the Faculty of Law, Medicine, Arts and Humanities and, belonging to the Faculty of Sciences and Technology, we have the Departments of Life Sciences, Physics, Chemistry, Mathematics and Architecture." +
                			"\n\n\tHowever, the Largo de D.Dinis still maintains itself as the main attraction point in this area of the University.");
                }
                
            }
        });

        ImageButton buttonPolo2 = (ImageButton) findViewById(R.id.polo2);
        buttonPolo2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	source="4";
                iniciarPercurso();
                
                infoGuide = (TextView) findViewById(R.id.info);
                if(language.equals("PT")){
                	infoGuide.setText("\tNeste percurso iremos visitar o PÓLO 2, um pouco mais afastado da zona centro, onde apenas se encontram os departamentos pertencentes à Faculdade de Ciências e Tecnologias nomedamente o Departamento de Engenharia Informática, de Engenharia Civil/Ambiente, de Engenharia Mecânica, de Engenharia Electrotécnica e de Engenharia Química. " +
                            "\n\n\tPodemos também destacar outros locais como as cantinas, as residências e o Edifício Central.");
                }
                else{
                	infoGuide.setText("\tIn this route we are going to visit the PÓLO 2, which is a little further from the center of the city." +
                			"\n\n\tHere we can only find departments that belong to the Faculty of Sciences and Technology such as the Departments of Informatics Engineering, Civil Engineering, Mechanical Engineering, Electrical and Computer Engineering and Chemical Engineering." +
                			"\n\n\tThere is also cafeterias, residencies and the Edifício Central.");
                }
                
            }
        });

    }

    private void iniciarPercurso(){

            button_play_info  = (ImageButton) findViewById(R.id.play_info);
            labelText = (TextView) findViewById(R.id.label_play_info);
        
            if(!labelText.getText().equals("COMEÇAR CIRCUITO") || !labelText.getText().equals("START ROUTE") ){
            	if(language.equals("PT")){
            		labelText.setText("COMEÇAR CIRCUITO");
            	}
            	else{
            		labelText.setText("START ROUTE");
            	}
            	
            	
                button_play_info.setImageResource(R.drawable.play);    
            }
    }

    private void maisInformacao(){
            button_play_info  = (ImageButton) findViewById(R.id.play_info);
            labelText = (TextView) findViewById(R.id.label_play_info);

            if(!labelText.getText().equals("MAIS INFORMAÇÃO") || !labelText.getText().equals("MORE INFO")){
                if(language.equals("PT")){
                	labelText.setText("MAIS INFORMAÇÃO");
                }
                else{
                	labelText.setText("MORE INFO");
                } 
                button_play_info.setImageResource(R.drawable.info);
            } 
    }

    /*COPY TO ACTIVITIES*/

    private void buildActionMenu(){
        ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.top_tab);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM);

        this.list = (ArrayList<PoI>) getIntent().getSerializableExtra("ListPoI");
        this.name=getIntent().getStringExtra("name");
    	this.language = getIntent().getStringExtra("language");
        
        TextView title = (TextView) findViewById(R.id.label_language);
        title.setTextColor(Color.rgb(255, 255, 255));
        
        ImageButton buttonSend = (ImageButton) findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String to = "ucheritage2013@gmail.com";
                String subject = "";
                String message = "";

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                email.setType("message/rfc822");
                if(language.equals("PT")){
                	startActivity(Intent.createChooser(email, "Escolhe um cliente de Email :"));
                }
                else{
                	startActivity(Intent.createChooser(email, "Choose an Email client :"));
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
            	Map();
                return true;
            case R.id.homeIcon:
                Home();
                return true;
            case R.id.about:
                About();
                return true;
            case R.id.settings:
            	Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void Map(){
    	Intent intent = new Intent(this,Map.class);
    	intent.putExtra("language",language);
    	intent.putExtra("source",""+source);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Search(String query){
    	 Intent intent = new Intent(this,Search.class);
    	 intent.putExtra("language",language);
    	 intent.putExtra("query", query);
    	 intent.putExtra("ListPoI",list);
    	 intent.putExtra("name",name);
         this.startActivity(intent);
    }

    private void About(){
        Intent intent = new Intent(this,About.class);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }
    
    private void Settings(){
    	Intent intent = new Intent(this,SignInActivity.class);
    	intent.putExtra("language",language);
    	intent.putExtra("ListPoI",list);
    	intent.putExtra("name",name);
        this.startActivity(intent);
    }

    private void Home(){
        Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("language",language);
        intent.putExtra("ListPoI",list);
        intent.putExtra("name",name);
        this.startActivity(intent);
        this.finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
    	if(language.equals("PT")){
    		getMenuInflater().inflate(R.menu.bot_tab_pt, menu);
    	}
    	else{
    		getMenuInflater().inflate(R.menu.bot_tab_en, menu);
    	}
    	
    	MenuItem searchItem = menu.findItem(R.id.search);
    	SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    	
    	final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
    	    @Override
    	    public boolean onQueryTextChange(String newText) {
    	        return true;
    	    }

    	    @Override
    	    public boolean onQueryTextSubmit(String query) { 
    	    	Search(query);
    	        return true;
    	    }
    	};

    	searchView.setOnQueryTextListener(queryTextListener);
    	
    	
    	
        return true;
    }

    /*------------------*/

}
